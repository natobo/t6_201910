package view;

import java.time.LocalDate;
import java.time.LocalDateTime;
import java.time.format.DateTimeFormatter;
import java.util.Iterator;
import java.util.Scanner;

import controller.Controller;
import model.data_structures.IteratorHash;
import model.vo.VOMovingViolations;

public class MovingViolationsManagerView 
{
	/**
	 * Constante con el nÃºmero maximo de datos maximo que se deben imprimir en consola
	 */
	public static final int N = 20;

	public void printMenu() {
		System.out.println("---------ISIS 1206 - Estructuras de datos----------");
		System.out.println("---------------------Taller 6----------------------");
		System.out.println("1. Cargar datos de infracciones en movimiento");
		System.out.println("2. Mostrar todas las infracciones que terminaron en un accidente en esa direcci�n con tabla de hash lineal Probing.");
		System.out.println("3. Mostrar todas las infracciones que terminaron en un accidente en esa direcci�n con tabla de hash Separate Chaining.");
		System.out.println("4. Salir");

		System.out.println("Digite el numero de opcion para ejecutar la tarea, luego presione enter: (Ej., 1):");

	}

	public void printDatosMuestra(  VOMovingViolations [ ] infracciones)
	{
		for ( VOMovingViolations elemento : infracciones)
		{	
			if(elemento!=null)
				System.out.println(  elemento.toString() );    
			
		}
	}
	public void printMessage(String mensaje) 
	{
		System.out.println(mensaje);
	}

}
