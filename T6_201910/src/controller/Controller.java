package controller;

import java.io.BufferedReader;
import java.io.File;
import java.io.FileNotFoundException;
import java.io.FileReader;
import java.io.IOException;
import java.time.LocalDateTime;
import java.time.format.DateTimeFormatter;
import java.util.Iterator;
import java.util.Scanner;

import com.google.gson.Gson;
import com.google.gson.GsonBuilder;
import com.google.gson.JsonObject;
import com.google.gson.stream.JsonReader;
import com.google.gson.stream.JsonToken;

import model.data_structures.ArregloDinamico;
import model.data_structures.HashLinearProbing;
import model.data_structures.HashSeparateChaining;
import model.data_structures.IteratorHash;
import model.data_structures.Queue;
import model.data_structures.IteratorQueue;
import model.util.Sort;
import model.vo.VOMovingViolations;
import view.MovingViolationsManagerView;

public class Controller 
{

	/**
	 * Lista de meses del programa
	 */
	private String[] listaMes=new String[6];
	/**
	 * View del programa
	 */
	private MovingViolationsManagerView view;
	/**
	 * Tabla de hash de tipo separate chaining.
	 */
	private HashSeparateChaining<Integer,Queue<VOMovingViolations>> tablaSC;
	/**
	 * Tabla de hash de tipo separate chaining.
	 */
	private HashLinearProbing<Integer,Queue<VOMovingViolations>> tablaLP;

	/**
	 * Constructor del controlador
	 */
	public Controller() 
	{
		view = new MovingViolationsManagerView();
		tablaSC=new HashSeparateChaining<>(1000);
		tablaLP = new HashLinearProbing<>(1000);

		listaMes[0]="January";
		listaMes[1]="February";
		listaMes[2]="March";
		listaMes[3]="April";
		listaMes[4]="May";
		listaMes[5]="June";
	}

	/**
	 * Leer los datos de las infracciones de los archivos. Cada infraccion debe ser Comparable para ser usada en los ordenamientos.
	 * Todas infracciones (MovingViolation) deben almacenarse en una Estructura de Datos (en el mismo orden como estan los archivos)
	 * A partir de estos datos se obtendran muestras para evaluar los algoritmos de ordenamiento
	 * @return numero de infracciones leidas 
	 */
	public int loadMovingViolations() 
	{
		int numDatos=0;
		for (int i = 0; i < 6; i++) 
		{
			String xMes=listaMes[i];
			try 
			{
				JsonReader lector=new JsonReader(new FileReader("."+File.separator+"data"+File.separator+"Moving_Violations_Issued_in_"+xMes+ "_2018.json"));

				ArregloDinamico<VOMovingViolations> x=crearArregloInfo(lector);

				System.out.println("Mes cargado: "+listaMes[i]);

				Queue<VOMovingViolations> infracciones=null;

				for (int j = 0; j < x.darTamano(); j++) 
				{
					Integer key=x.darElemento(j).getAddressId();
					if(key!=-1)
					{
						if(tablaSC.contains(key))
						{
							infracciones=tablaSC.get(key);
							infracciones.enqueue(x.darElemento(j));
							tablaSC.put(key, infracciones);
						}
						else
						{
							infracciones=new Queue<VOMovingViolations>();
							infracciones.enqueue(x.darElemento(j));
							tablaSC.put(key, infracciones);
						}
						if(tablaLP.contains(key))
						{
							infracciones=tablaLP.get(key);
							infracciones.enqueue(x.darElemento(j));
							tablaLP.put(key, infracciones);
						}
						else
						{
							infracciones=new Queue<VOMovingViolations>();
							infracciones.enqueue(x.darElemento(j));
							tablaLP.put(key, infracciones);
						}
					}
				}

				numDatos+=x.darTamano();

				lector.close();
			}
			catch (Exception e) 
			{
				// TODO: handle exception
				e.printStackTrace();
			}	
		}

		System.out.println("Numero de llaves: "+tablaSC.numKeys());

		return numDatos;
	}


	public void InfraccioneshashlinealProbing( int pDireccion)
	{
		Queue<VOMovingViolations> infracciones = tablaLP.get(pDireccion);

		if(infracciones!=null)
		{
			IteratorQueue<VOMovingViolations> it = infracciones.iterator();

			VOMovingViolations[ ] arregloTmp= new VOMovingViolations [infracciones.size()];

			int i=0;
			while(it.hasNext())
			{
				VOMovingViolations element=it.next();

				if(element.getAccidentIndicator().equals("Yes"))
				{
					System.out.println(element.getAddressId());
					arregloTmp[i]=element;
					i++;
				}

			}			

			Sort.ordenarMergeSort(arregloTmp, "address_ID");

			if ( arregloTmp != null)
			{ 

				view.printDatosMuestra(arregloTmp);
			} 

		}
	}

	public void InfraccioneshashSeparateChaining(int pDireccion)
	{
		Queue<VOMovingViolations> infracciones=tablaSC.get(pDireccion);

		if(infracciones!=null)
		{
			IteratorQueue<VOMovingViolations> it=infracciones.iterator();

			VOMovingViolations[ ] arregloTmp= new VOMovingViolations [infracciones.size()];

			int i=0;
			while(it.hasNext())
			{
				VOMovingViolations element=it.next();

				if(element.getAccidentIndicator().equals("Yes"))
				{
					System.out.println(element.getAddressId());
					arregloTmp[i]=element;
					i++;
				}

			}			

			Sort.ordenarMergeSort(arregloTmp, "address_ID");

			if ( arregloTmp != null)
			{ 

				view.printDatosMuestra(arregloTmp);
			} 

		}
	}

	private ArregloDinamico<VOMovingViolations> crearArregloInfo(JsonReader reader) throws IOException
	{
		ArregloDinamico<VOMovingViolations> arreglo=new ArregloDinamico<VOMovingViolations>(30000000);
		reader.beginArray();
		while (reader.hasNext()) 
		{
			arreglo.agregar(crearInfraccion(reader));
		}
		reader.endArray();
		return arreglo;
	}

	private VOMovingViolations crearInfraccion(JsonReader lector) throws IOException
	{
		String ObjectId="";
		String ViolationDescription="";
		String Location="";
		String TotalPaid="";
		String AccidentIndicator="";
		LocalDateTime TicketIssueDate=null;
		String ViolationCode="";
		String FineAMT="";
		Integer Street=0;
		Integer Address_Id=0;
		String Penalty1="";
		String Penalty2="";

		lector.beginObject();
		while (lector.hasNext())
		{
			if(lector.peek() != JsonToken.NULL)
			{
				String info = lector.nextName();
				//						System.out.println(info);
				if (info.equals("OBJECTID")) 
				{
					ObjectId= lector.nextString();	
				} 
				else if (info.equals("LOCATION")) 
				{
					Location = lector.nextString();
				} 
				else if (info.equals("ADDRESS_ID")) 
				{
					try 
					{
						Address_Id = lector.nextInt();
					} 
					catch (Exception e) 
					{
						// TODO: handle exception
						Address_Id=-1;
					}
					//					System.out.println("Adr::::"+Address_Id);
				}  
				else if (info.equals("STREETSEGID")) 
				{
					try 
					{
						Street = lector.nextInt();
					} 
					catch (Exception e) 
					{
						// TODO: handle exception
						Street=-1;
					}
				} 
				else if (info.equals("VIOLATIONDESC")) 
				{
					ViolationDescription = lector.nextString();
				} 
				else if (info.equals("TOTALPAID")) 
				{
					TotalPaid = lector.nextString();
				} 
				else if (info.equals("ACCIDENTINDICATOR")) 
				{
					AccidentIndicator = lector.nextString();
				}
				else if (info.equals("TICKETISSUEDATE")) 
				{
					TicketIssueDate = convertirFecha_Hora_LDT(lector.nextString());
				}
				else if (info.equals("VIOLATIONCODE")) 
				{
					ViolationCode = lector.nextString();
				}
				else if (info.equals("FINEAMT")) 
				{
					FineAMT = lector.nextString();
				}
				else if (info.equals("PENALTY1")) 
				{
					Penalty1 = lector.nextString();
				}
				else if (info.equals("PENALTY2")) 
				{
					try 
					{
						Penalty2 = lector.nextString();
					} 
					catch (Exception e) 
					{
						// TODO: handle exception
						Penalty2="";
					}
				}
				else 
				{
					lector.skipValue();
				}
			}
			else
			{
				lector.skipValue();
			}
		}
		lector.endObject();
		VOMovingViolations infraccion= new VOMovingViolations(ObjectId, ViolationDescription, Location, TotalPaid, AccidentIndicator, TicketIssueDate, ViolationCode, FineAMT, Street, Address_Id, Penalty1, Penalty2);
		return infraccion;

	} 

	/**
	 * Metodo que corre el programa 
	 */
	public void run() 
	{
		// TODO Auto-generated method stub
		long startTime;
		long endTime;
		long duration;

		int nDatos = 0;
		int nMuestra = 0;

		Scanner sc = new Scanner(System.in);
		boolean fin = false;

		while(!fin)
		{
			view.printMenu();

			int option = sc.nextInt();

			switch(option)
			{
			case 1:
				// Cargar infracciones
				nDatos=this.loadMovingViolations();
				view.printMessage("Numero infracciones cargadas:" + nDatos);
				break;
			case 2:
				// Cargar infracciones
				view.printMessage("Dar direccion (addressId): ");
				nMuestra = sc.nextInt();
				InfraccioneshashlinealProbing(nMuestra);

				break;
			case 3:
				// Cargar infracciones
				view.printMessage("Dar direccion (addressId): ");
				nMuestra = sc.nextInt();
				InfraccioneshashSeparateChaining(nMuestra);
				break;
			case 4:
				fin=true;
				sc.close();
				break;
			}
		}
	}

	/**
	 * Convertir fecha y hora a un objeto LocalDateTime
	 * @param fecha fecha en formato dd/mm/aaaaTHH:mm:ss con dd para dia, mm para mes y aaaa para agno, HH para hora, mm para minutos y ss para segundos
	 * @return objeto LDT con fecha y hora integrados
	 */
	private static LocalDateTime convertirFecha_Hora_LDT(String fechaHora)
	{
		return LocalDateTime.parse(fechaHora, DateTimeFormatter.ofPattern("yyyy-MM-dd'T'HH:mm:ss'.000Z'"));
	}
}	
