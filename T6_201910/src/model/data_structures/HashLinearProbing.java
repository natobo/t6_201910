package model.data_structures;

import java.util.Iterator;

public class HashLinearProbing<K extends Comparable<K>,V> implements IHash<K,V> {

	/**
	 * Arreglo con la llaves
	 */
	private K[] llaves;
	/**
	 * Arreglo con los valores
	 */
	private V[] valores;
	
	/**
	 *Numero de llaves en la tabla
	 */
	private int numLlaves;
	/**
	 *tama�o de la tabla
	 */
	private int tama�o = 16;

	@SuppressWarnings("unchecked")
	public HashLinearProbing(int pTama�o){
		llaves = (K[]) new Comparable[tama�o];
		valores = (V[]) new Comparable[tama�o];
	}
	
	private int hash(K key){
		return (key.hashCode() & 0x7fffffff) % tama�o;
	}
	
	private void resize(int x){
		HashLinearProbing<K,V> t;
		t = new HashLinearProbing<K,V>(x);
		for(int i = 0; i < tama�o; i++){
			if(llaves[i] != null) t.put(llaves[i], valores[i]);
		}
		llaves = t.llaves;
		valores = t.valores;
		tama�o = t.tama�o;
	}
	
	@Override
	public void put(K key, V value) {
		// TODO Auto-generated method stub
		if(numLlaves >= tama�o/2) resize(2*tama�o);
		
		int i;
		for(i = hash(key); llaves[i] != null; i = (i+1)%tama�o){
			if(llaves[i].equals(key)){valores[i] = value; return;}
		}
		llaves[i] = key;
		valores[i] = value;
		numLlaves ++;
	}

	@Override
	public V get(K key) {
		// TODO Auto-generated method stub
		for(int i = hash(key); llaves[i] != null; i = (i+1)%tama�o){
			if(llaves[i].equals(key)) return valores[i];
		}
		return null;
	}

	@Override
	public V delete(K key) {
		// TODO Auto-generated method stub
		for(int i = hash(key); llaves[i] != null; i = (i+1)%tama�o){
			if(llaves[i].equals(key)){
				llaves[i] = null;
				return valores[i];
			}
		}
		return null;
	}

	@Override
	public Iterator<K> keys() {
		// TODO Auto-generated method stub
		return null;
	}

	@Override
	public int size() {
		// TODO Auto-generated method stub
		return tama�o;
	}

	@Override
	public boolean contains(K key) {
		// TODO Auto-generated method stub
		for(int i = hash(key); llaves[i] != null; i = (i+1)%tama�o){
			if(llaves[i].equals(key)) return true;
		}
		return false;
	}

	@Override
	public int numKeys() {
		// TODO Auto-generated method stub
		return llaves.length;
	}

}
