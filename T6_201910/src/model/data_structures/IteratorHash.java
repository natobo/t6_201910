package model.data_structures;

import java.util.Iterator;
import java.util.NoSuchElementException;
/**
 * Clase que representa un iterador 
 * @author nicot
 * @param <V>
 * @param <T>
 */
public class IteratorHash<K, V> implements Iterator<K>
{
	/**
	 * Nodo que representa al proximo del actual
	 */
	private NodoTabla<K,V> proximo;
	
	/**
	 * Construye el iterador y asigna el nodo proximo a un nodo inicial para comenzar la iteracion.
	 * @param primero
	 */
	public IteratorHash(NodoTabla<K,V> primero) 
	{
		// TODO Auto-generated constructor stub
		proximo=primero;
	}

	@Override
	public boolean hasNext() 
	{
		// TODO Auto-generated method stub
		return proximo!=null;
	}

	@Override
	public K next() 
	{
		// TODO Auto-generated method stub
		if ( proximo == null )
		{ 
			throw new NoSuchElementException(); 
		}
		K elemento = proximo.getLlave(); // ultimo elemento visitado
		proximo = proximo.getSiguiente(); // proximo nodo a visitar
		return elemento;
	}

}
