package model.util;

import java.util.Iterator;

import junit.framework.TestCase;
import model.data_structures.HashLinearProbing;


public class TestLinearProbing extends TestCase{

	/**
	 * Tabla hashSeparateChaining que va realizar pruebas sobre tuplas integers-Strings.
	 */
	private HashLinearProbing<Integer,String> tablaIntegers;
	/**
	 * Tabla hashSeparateChaining que va realizar pruebas sobre tuplas integers-Strings.
	 */
	private HashLinearProbing<String,Integer> tablaStrings;


	/**
	 * Monta el escenario para probar las tablas  de hash 
	 * @throws Exception
	 */
	public void setUp() throws Exception
	{

		tablaIntegers=new HashLinearProbing<>(1);
		tablaIntegers.put(1,"numero1");
		tablaIntegers.put(2,"numero2");
		tablaIntegers.put(3,"numero3");
		tablaIntegers.put(4,"numero4");
		tablaIntegers.put(5,"numero5");

		tablaStrings=new HashLinearProbing<>(1);
		tablaStrings.put("A",1);	
		tablaStrings.put("B",2);
		tablaStrings.put("C",3);
		tablaStrings.put("D",4);
		tablaStrings.put("E",5);

	}
	
	public void setUp2(){
		tablaIntegers = new HashLinearProbing<>(1);
		tablaStrings = new HashLinearProbing<>(1);
	}

	public void testPut(){
		try{
			setUp2();
			
			tablaIntegers.put(1, "valor1");
			tablaIntegers.put(2, "valor2");
			tablaIntegers.put(3, "valor3");
			
			assertEquals("El tama�o deberia ser 3", 3 ,tablaIntegers.size());
			
			tablaStrings.put("llave1", 1);
			tablaStrings.put("llave2", 2);
			tablaStrings.put("llave3", 3);
			
			assertEquals("El tama�o deberia ser 3", 3 ,tablaStrings.size());
		}
		catch(Exception e){
			
		}
	}
	
	/**
	 * Prueba que el metodo contains de las tablas si agregue los nodos con los elementos
	 * @throws Exception 
	 */
	public void testContains()
	{
		try 
		{
			setUp();

			//revisa que si agregue los elementos
			assertTrue("La tabla deberia contener el elemento 1 ",tablaIntegers.contains(1));
			assertTrue("La tabla deberia contener el elemento 2 ",tablaIntegers.contains(2));
			assertTrue("La tabla deberia contener el elemento 3 ",tablaIntegers.contains(3));
			assertTrue("La tabla deberia contener el elemento 4 ",tablaIntegers.contains(4));
			assertTrue("La tabla deberia contener el elemento 5 ",tablaIntegers.contains(5));

			assertTrue("La tabla deberia contener el elemento 1 ",tablaStrings.contains("A"));
			assertTrue("La tabla deberia contener el elemento 1 ",tablaStrings.contains("B"));
			assertTrue("La tabla deberia contener el elemento 1 ",tablaStrings.contains("C"));
			assertTrue("La tabla deberia contener el elemento 1 ",tablaStrings.contains("D"));
			assertTrue("La tabla deberia contener el elemento 1 ",tablaStrings.contains("E"));

		} 

		catch (Exception e) 
		{
			// TODO Auto-generated catch block
		}
	}
	/**
	 * Prueba el metodo get de las tablas , donde se retorna el valor dada una llave.
	 */
	public void testGet()
	{
		try 
		{
			setUp();

			assertEquals("Deberia retornar el string correctamente","numero1",tablaIntegers.get(1));
			assertEquals("Deberia retornar el string correctamente","numero2",tablaIntegers.get(2));
			assertEquals("Deberia retornar el string correctamente","numero3",tablaIntegers.get(3));
			assertEquals("Deberia retornar el string correctamente","numero4",tablaIntegers.get(4));
			assertEquals("Deberia retornar el string correctamente","numero5",tablaIntegers.get(5));

			assertEquals("Deberia retornar el int correctamente",1,(int)tablaStrings.get("A"));
			assertEquals("Deberia retornar el int correctamente",2,(int)tablaStrings.get("B"));
			assertEquals("Deberia retornar el int correctamente",3,(int)tablaStrings.get("C"));
			assertEquals("Deberia retornar el int correctamente",4,(int)tablaStrings.get("D"));
			assertEquals("Deberia retornar el int correctamente",5,(int)tablaStrings.get("E"));
		} 
		catch (Exception e) 
		{
			// TODO Auto-generated catch block

			e.printStackTrace();
		}
	}

	/**
	 * Metodo que revisa que cuando se agregue una llave con null se elimine la llave de la tabla, y que no se pueda agregar un objeto con llave y valor nulo.
	 */
	public void testPutNull()
	{
		try 
		{
			setUp();
			tablaIntegers.put(null,null);
		} 
		catch (Exception e) 
		{
			// TODO Auto-generated catch block
			assertTrue("Deberia lanzar excepcion",true);
		}
		try 
		{
			setUp();
			tablaStrings.put(null,null);
		} 
		catch (Exception e) 
		{
			// TODO Auto-generated catch block
			assertTrue("Deberia lanzar excepcion",true);
		}
	}
	/**
	 * Metodo que prueba que se elimine de la tabla un nodo por su llave
	 */
	public void testDelete()
	{
		try 
		{
			setUp();

			tablaIntegers.delete(1);
			assertFalse("No deberia estar dentro de la tabla", tablaIntegers.contains(1));
			assertFalse("No deberia retornar ningun Valor", tablaIntegers.get(1)!=null);

			tablaStrings.delete("A");
			assertFalse("No deberia estar dentro de la tabla", tablaStrings.contains("A"));
			assertFalse("No deberia retornar ningun Valor", tablaStrings.get("A")!=null);
		} 
		catch (Exception e) 
		{
			// TODO Auto-generated catch block
			//e.printStackTrace();
		}
	}
	/**
	 * Metodo que prueba que la tabla reacomode su tama�o si supera su factor de carga
	 */
	public void testRehash()
	{
		try 
		{
			setUp();
			// Hay 5 llaves dentro de un solo arreglo
			assertEquals("El tama�o de la tabla deberia ser 1",1,(int)tablaIntegers.size());

			tablaIntegers.put(6,"numero6");

			assertEquals("El tama�o de la tabla deberia ser 2",2,(int)tablaIntegers.size());

			assertEquals("El tama�o de la tabla deberia ser 1",1,(int)tablaStrings.size());

			tablaStrings.put("F",6);
			
			assertEquals("El tama�o de la tabla deberia ser 2",2,(int)tablaStrings.size());

		} 
		catch (Exception e) 
		{
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
	}
	/**
	 * Metodo que prueba que se retorna un iterador sobre todas los nodos de la tabla
	 */
	public void testKeys()
	{
		try 
		{
			setUp();
			int contIteracionesInt=0;
			Iterator<Integer> it=tablaIntegers.keys();
			while (it.hasNext()) 
			{
				contIteracionesInt++;
				it.next();
			}
			assertEquals("El Contador de iteraciones debio quedar en 5",5,contIteracionesInt);
			
			int contIteracionesStr=0;
			Iterator<String> it2=tablaStrings.keys();
			while (it2.hasNext()) 
			{
				contIteracionesStr++;
				it2.next();
			}
			assertEquals("El Contador de iteraciones debio quedar en 5",5,contIteracionesStr);
			
		} catch (Exception e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
	
		
	}
	
}
